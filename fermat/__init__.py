from fermat.Fermat import Fermat
from fermat.clusterize import FermatKMeans
from fermat import path_methods

__all__ = ['Fermat', 'FermatKMeans']
